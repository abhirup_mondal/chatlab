import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const ChatData = new Mongo.Collection('chatdata');

ChatData.allow({
    insert: () => false,
    remove: () => false,
    update: () => false,
});

ChatData.deny({
    insert: () => true,
    remove: () => true,
    update: () => true,
});

const MessageSchema = new SimpleSchema({
    'messageString': {
        type: String,
        label: 'The contents of the message',
    },
    'timeStamp': {
        type: Date,
        label: 'The time the message was sent',
    },
    'sender': {
        type: String,
        label: 'The user-id of the sender of this message',
    }
})

let ChatDataSchema = new SimpleSchema({
    'chatTitle': {
        type: String,
        label: 'The title of a chat',
    },
    'chatType': {
        type: String,
        label: 'Type of chat: Group Chat or Simple Chat',
    },
    'participants': {
        type: [String],
        label: 'The list of user-ids who are participating in this chat',
    },
    'messages': {
        type: [MessageSchema]
    },
    'admin': {
        type: [String],
        label: 'The admins of the group in case this is a group chat'
    },
    'last-message-ts': {
        type: Date,
        label: 'Timestamp of the last sent message'
    },
});

ChatData.attachSchema(ChatDataSchema);
