import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const UnreadData = new Mongo.Collection('unreaddata');

UnreadData.allow({
    insert: () => false,
    remove: () => false,
    update: () => false,
});

UnreadData.deny({
    insert: () => true,
    remove: () => true,
    update: () => true,
});

let UnreadDataSchema = new SimpleSchema({
    'userID': {
        type: String,
        label: 'The unique identifier of a user',
    },
    'chatID': {
        type: String,
        label: 'The unique identifier of a Chat',
    },
    'unreadCount': {
        type: Number,
        label: 'The number of messages unread by userID for chatID',
    }
});

UnreadData.attachSchema(UnreadDataSchema);
