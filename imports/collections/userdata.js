import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const UserData = new Mongo.Collection('userdata');

UserData.allow({
    insert: () => false,
    remove: () => false,
    update: () => false,      
});

UserData.deny({
    insert: () => true,
    remove: () => true,
    update: () => true,
});

let UserDataSchema = new SimpleSchema({
    'userID': {
        type: String,
        label: 'The unique identifier of the User',
    },
    'contacts': {
        type: [String],
        label: 'The list of user-ids who are contacts of the user',
    },
    'chats': {
        type: [String],
    },
});

UserData.attachSchema(UserDataSchema);
