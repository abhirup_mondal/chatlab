import { Meteor } from 'meteor/meteor';
import { Templating } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';


import { UserData } from '../../collections/userdata.js';

import '../../methods.js';
import './user-search-template.html';

Template['user-search-template'].onCreated(function () {
    this.searchQuery = new ReactiveVar('');
    this.searching = new ReactiveVar(false);
});

Template['user-search-template'].helpers({

    searchQuery() {
        return Template.instance().searchQuery.get();
    },

    searching() {
        return Template.instance().searching.get();
    },

    foundContacts() {
        let query = {},
            projection = { limit: 10 };
        let search = Template.instance().searching.get();
        console.log("Do Search: " + search);
        if (search) {
            let reg = '.*' + Template.instance().searchQuery.get() + '.*';

            query = {
                '$or': [
                    { 'emails.0.address': { '$regex': reg, '$options': 'i' } },
                    { 'profile.name': { '$regex': reg, '$options': 'i' } }
                ]
            };

            projection.limit = 100;
        }

        return Meteor.users.find(query, projection, {'$sort': {'profile': {'name': -1}}});
    },

    userData() {
        return Meteor.user();
    },
});

Template['user-search-template'].events({

    'keyup #user-search': function (event, template) {
        event.preventDefault();

        var inputQuery = event.target.value;

        if (inputQuery !== '') {
            template.searchQuery.set(inputQuery);
            template.searching.set(true);
        }

        if (inputQuery === '') {
            template.searchQuery.set(inputQuery);
        }
    },

    'submit #user-search': function (event, template) {
        event.preventDefault();

        var inputQuery = event.target.value;

        if (inputQuery !== '') {
            template.searchQuery.set(inputQuery);
            template.searching.set(true);
        }

        if (inputQuery === '') {
            template.searchQuery.set(inputQuery);
        }
    },

    'click .add-contact-button': function (event, template) {
        var contactID = event.currentTarget.id;
        Meteor.call('addContact', contactID);
    },

    'click #back-button': function(event, template) {
        BlazeLayout.render('primary', { main: 'chat-template', sidebar: 'chat-list-template' });
    }
})


