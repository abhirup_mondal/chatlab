import { Meteor } from 'meteor/meteor';
import { Templating } from 'meteor/templating';

import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import { UserData } from '../../collections/userdata.js';

import '../../methods.js';
import './add-group-template.html';

Template['add-group-template'].onCreated(function () {
    this.searchQuery = new ReactiveVar('');
    this.searching = new ReactiveVar(false);

    this.selectedArray = new ReactiveDict();
});

Template['add-group-template'].helpers({

    searchQuery() {
        return Template.instance().searchQuery.get();
    },

    searching() {
        return Template.instance().searching.get();
    },

    foundContacts() {
        let query = {},
            projection = { limit: 10 };
        let search = Template.instance().searching.get();
        console.log("Do Search: " + search);
        if (search) {
            let reg = '.*' + Template.instance().searchQuery.get() + '.*';

            query = {
                '$or': [
                    { 'emails.0.address': { '$regex': reg, '$options': 'i' } },
                    { 'profile.name': { '$regex': reg, '$options': 'i' } }
                ]
            };

            projection.limit = 100;
        }

        return Meteor.users.find(query, projection, { '$sort': { 'profile': { 'name': -1 } } });
    },

    userData() {
        return Meteor.user();
    },
});

Template['add-group-template'].events({

    'keyup #user-search': function (event, template) {
        event.preventDefault();

        var inputQuery = event.target.value;

        if (inputQuery !== '') {
            template.searchQuery.set(inputQuery);
            template.searching.set(true);
        }

        if (inputQuery === '') {
            template.searchQuery.set(inputQuery);
        }
    },

    'submit #user-search': function (event, template) {
        event.preventDefault();

        var inputQuery = event.target.value;

        if (inputQuery !== '') {
            template.searchQuery.set(inputQuery);
            template.searching.set(true);
        }

        if (inputQuery === '') {
            template.searchQuery.set(inputQuery);
        }
    },

    'click .add-contact-button': function (event, template) {

        var classes = event.currentTarget.className;
        var classArr = classes.split(' ');
        var contactID = event.currentTarget.id;
        contactID = contactID.substring(2);
        
        template.selectedArray.set(contactID, true);
        $(event.currentTarget).addClass('not-show');
        $('#d-'+contactID).removeClass('not-show');

        console.log(template.selectedArray);
    },

    'click .del-contact-button': function (event, template) {

        var classes = event.currentTarget.className;
        var classArr = classes.split(' ');
        var contactID = event.currentTarget.id;
        contactID = contactID.substring(2);
        
        template.selectedArray.set(contactID, false);
        $(event.currentTarget).addClass('not-show');
        $('#a-'+contactID).removeClass('not-show');

        console.log(template.selectedArray);
    },

    'click #create-new-group': function(event, template) {
        event.preventDefault();

        var selectedContacts = [];
        for (var key in template.selectedArray.keys) {
            if (template.selectedArray.get(key)) {
                selectedContacts.push(key);
            }
        }

        selectedContacts.push(Meteor.userId());

        var groupName = $('#new-group').val();
        if (groupName === '') {
            alert('Please enter a Group Name');
            return
        }

        Meteor.call('addNewGroupChat', groupName, selectedContacts);
    },

    'click #back-button': function (event, template) {
        BlazeLayout.render('primary', { main: 'chat-template', sidebar: 'chat-list-template' });
    }
})


