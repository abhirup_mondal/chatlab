import { Meteor } from 'meteor/meteor';
import { Templating } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';

import { ChatData } from '../../collections/chatdata.js'
import { UserData } from '../../collections/userdata.js';

import './chat-list-template.html';

Template['chat-list-template'].onCreated(function () {
    this.searchQuery = new ReactiveVar('');
    this.searching = new ReactiveVar(false);

    Meteor.call('addCurrentUser');
});

Template['chat-list-template'].helpers({

    searchQuery() {
        return Template.instance().searchQuery.get();
    },

    searching() {
        return Template.instance().searching.get();
    },

    foundChats() {
        var userData = UserData.findOne({'userID': Meteor.userId()});
        var userChatIDs = userData['chats'];

        var userChats = []
        for (var i = 0; i < userChatIDs.length; ++i) {
            var currentID = userChatIDs[i];
            userChats.push(ChatData.findOne({ '_id': currentID }));
        }

        userChats.sort(function(a,b) {
            var dateA = new Date(a['last-message-ts']);
            var dateB = new Date(b['last-message-ts']);
            return dateB - dateA;
        });

        chatTitles = [];

        for (var i = 0; i < userChats.length; ++i) {
            var currentID = userChats[i]['_id'];
            var chatData = ChatData.findOne({ '_id':currentID });
            var currentChatTitle = '';
            if (chatData['chatType'] === 'Simple') {
                var currentChatParticipants = chatData['participants'];
                if (currentChatParticipants[0] === Meteor.userId()) {
                    var participantID = currentChatParticipants[1];
                    var userDoc = Meteor.users.findOne(
                        {'_id': participantID}
                    );
                    var currentChatTitle = userDoc['profile']['name'];
                }
                else {
                    var participantID = currentChatParticipants[0];
                    var userDoc = Meteor.users.findOne(
                        { '_id': participantID }
                    );
                    var currentChatTitle = userDoc['profile']['name'];
                }
            }
            else {
                currentChatTitle = chatData['chatTitle'];
            }

            var messages = chatData['messages'];
            var lastMessage = 'No Message';
            if (messages.length > 0) {
                lastMessage = messages[messages.length - 1]['messageString'];
            }

            chatTitles.push({
                'title': currentChatTitle,
                'lm': lastMessage,
                'ts': chatData['last-message-ts'],
                'id': currentID
            });
        }

        return chatTitles;
    },

    userData() {
        return Meteor.user();
    },
});

Template['chat-list-template'].events({
    // 'keyup #chat-search': function (event, template) {
    //     event.preventDefault();

    //     var inputQuery = event.target.value;

    //     if (inputQuery !== '') {
    //         template.searchQuery.set(inputQuery);
    //         template.searching.set(true);
    //     }

    //     if (inputQuery === '') {
    //         template.searchQuery.set(inputQuery);
    //     }
    // },

    // 'submit #contact-search': function (event, template) {
    //     event.preventDefault();

    //     var inputQuery = event.target.value;

    //     if (inputQuery !== '') {
    //         template.searchQuery.set(inputQuery);
    //         template.searching.set(true);
    //     }

    //     if (inputQuery === '') {
    //         template.searchQuery.set(inputQuery);
    //     }
    // }

    'mouseenter .chat-names': function (event, template) {
        event.preventDefault();

        console.log(event.target.id);
        var selector = '#' + event.target.id;

        $(selector).addClass('uk-card-primary');
        $(selector).removeClass('uk-card-default');

        console.log('click');
    },

    'mouseleave .chat-names': function (event, template) {
        event.preventDefault();

        console.log(event.target.id);
        var selector = '#' + event.target.id;

        $(selector).removeClass('uk-card-primary');
        $(selector).addClass('uk-card-default');

        console.log('click');
    },

    'click .chat-names': function(event, template) {
        event.preventDefault();

        console.log(event.currentTarget.className);
        var selector = '#' + event.currentTarget.id;

        $('.chat-names').addClass('uk-card-default');
        $('.chat-names').removeClass('uk-card-primary');
        $(selector).addClass('uk-card-primary');
        $(selector).removeClass('uk-card-default');
        

        FlowRouter.go('/chat/'+event.target.id);

        console.log('click');
    },

    'click #back-button': function(event, template) {
        FlowRouter.go('/');
    }
})




