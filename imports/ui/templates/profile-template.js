import { Meteor } from 'meteor/meteor';
import './profile-template.html';

Template['profile-template'].helpers({
    userData() {
        return Meteor.user();
    }
});
