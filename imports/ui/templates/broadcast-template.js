import { Meteor } from 'meteor/meteor';
import { Templating } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';


import { UserData } from '../../collections/userdata.js';

import '../../methods.js';
import './broadcast-template.html';

Template['broadcast-template'].helpers({
    userData() {
        return Meteor.user();
    },
});

Template['broadcast-template'].events({

    'click #broadcast-button': function (event, template) {
        event.preventDefault();

        var broadcastMsg = $('#new-broadcast').val();
        if (broadcastMsg === '') {
            alert('Please Enter a Message');
            return;
        }

        Meteor.call('broadcast', broadcastMsg);

        BlazeLayout.render('primary', { main: 'chat-template', sidebar: 'chat-list-template' });
    },

    'click #back-button': function (event, template) {
        BlazeLayout.render('primary', { main: 'chat-template', sidebar: 'chat-list-template' });
    }
})
