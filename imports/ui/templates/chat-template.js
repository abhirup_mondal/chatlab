import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';

import '../../methods.js';


import './chat-template.html';

import './add-group-template.js';
import './broadcast-template.js';
import './chat-list-template.js';
import './chat-view-template.js';
import './user-search-template.js';

Template['chat-template'].onCreated(function() {

});

Template['chat-template'].helpers({
    userData() {
        return Meteor.user();
    },
});

Template['chat-template'].events({

    'click #add-contact-button': function(event, template) {
        BlazeLayout.render('primary', { main: 'chat-template', sidebar: 'user-search-template' });
    },

    'click #create-group': function(event, template) {
        BlazeLayout.render('primary', { main: 'chat-template', sidebar: 'add-group-template' });
    },

    'click #broadcast': function(event, template) {
        BlazeLayout.render('primary', { main: 'chat-template', sidebar: 'broadcast-template' });
    }
})

