import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';

import { ChatData } from '../../collections/chatdata.js';

import './chat-view-template.html';

Template['chat-view-template'].helpers({

    chatTitle() {
        var currentID = FlowRouter.getParam('chatID');
        var chatData = ChatData.findOne({ '_id': currentID });
        var currentChatTitle = '';
        if (chatData['chatType'] === 'Simple') {
            var currentChatParticipants = chatData['participants'];
            if (currentChatParticipants[0] === Meteor.userId()) {
                var participantID = currentChatParticipants[1];
                var userDoc = Meteor.users.findOne(
                    { '_id': participantID }
                );
                var currentChatTitle = userDoc['profile']['name'];
            }
            else {
                var participantID = currentChatParticipants[0];
                var userDoc = Meteor.users.findOne(
                    { '_id': participantID }
                );
                var currentChatTitle = userDoc['profile']['name'];
            }
        }
        else {
            currentChatTitle = chatData['chatTitle'];
        }

        return currentChatTitle;
    },

    chatMessages() {
        var currentID = FlowRouter.getParam('chatID');
        var chatData = ChatData.findOne({ '_id': currentID });
        console.log(chatData);
        var chatMsgs = chatData['messages'];
        return chatMsgs;
    },

    getClass(userID) {
        if (userID == Meteor.userId()) {
            return 'right-chat';
        }
        else {
            return 'left-chat';
        }
    },

    getSenderName(senderID) {
        var senderDoc = Meteor.users.findOne({'_id': senderID});
        console.log('SenderDoc' + senderDoc);
        return senderDoc['profile']['name'];
    }

})

Template['chat-view-template'].events({

    'click #send-text': function(event, template) {
        event.preventDefault();

        var chatID = FlowRouter.getParam('chatID');
        var messageText = $('#chat-text').val();
        Meteor.call('addTextToChat', chatID, messageText,Meteor.userId());

        $('#chat-text').val('');
    }
})
