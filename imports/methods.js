import { ChatData } from './collections/chatdata.js';
import { UserData } from './collections/userdata.js';

Meteor.methods({

    addCurrentUser: function() {
        var doc = UserData.findOne({'userID': Meteor.userId()});
        if (!doc) {
            UserData.insert(
                { 'userID': Meteor.userId(), 'contacts': [], 'chats': [] }
            );
        }
    },

    addUser: function(userID) {
        var doc = UserData.findOne({ 'userID': userID });
        if (!doc) {
            UserData.insert(
                { 'userID': userID, 'contacts': [], 'chats': [] }
            );
        }
    },

    addContact: function(contactID) {
        Meteor.call('addCurrentUser');
        UserData.update(
            { 'userID': Meteor.userId() },
            { '$addToSet': { 'contacts': { '$each': [contactID]} } }
        );

        Meteor.call('addUser', contactID);
        UserData.update(
            { 'userID': contactID },
            { '$addToSet': { 'contacts': { '$each': [Meteor.userId()] } } }
        );

        Meteor.call('addPersonalChat', Meteor.userId(), contactID);
    },

    addPersonalChat: function(userID_A, userID_B) { 
        var doc = ChatData.findOne({ 'participants': [userID_A, userID_B] });
        if (!doc) {
            doc = ChatData.findOne({ 'participants': [userID_B, userID_A] });
        }
        if (!doc) {
            ChatData.insert({
                'chatTitle'      : 'N/A',
                'chatType'       : 'Simple',
                'participants'   : [userID_A, userID_B],
                'messages'       : [],
                'admin'          : [],
                'last-message-ts': new Date()
            });
        }

        var addedChat = ChatData.findOne({ 'participants': [userID_A, userID_B] });
        var addedChatID = addedChat._id;

        Meteor.call('addUser', userID_A);
        Meteor.call('addUser', userID_B);

        UserData.update(
            { 'userID': userID_A },
            { '$addToSet': { 'chats': { '$each': [addedChatID] } } }
        );

        UserData.update(
            { 'userID': userID_B },
            { '$addToSet': { 'chats': { '$each': [addedChatID] } } }
        );
    },

    addTextToChat: function(chatID, message, senderID) {

        var messageDoc = {};
        var ts = new Date();
        messageDoc['messageString'] = message;
        messageDoc['sender'] = senderID;
        messageDoc['timeStamp'] = ts;

        ChatData.update(
            { '_id': chatID },
            {
                '$push': { 
                    'messages': {
                        '$each': [messageDoc],
                    }
                }
            }
        );

        ChatData.update(
            { '_id': chatID },
            { '$set': { 'last-message-ts': ts } }
        );
    },

    addNewGroupChat: function(groupName, participants) {
        var newGroupChatID = ChatData.insert({
            'chatTitle': groupName,
            'chatType': 'Group',
            'participants': participants,
            'messages': [],
            'admin': [Meteor.userId()],
            'last-message-ts': new Date()
        });

        console.log(newGroupChatID);

        for (var i = 0; i < participants.length; ++i) {
            Meteor.call('addUser', participants[i]);

            UserData.update(
                { 'userID': participants[i] },
                { '$addToSet': { 'chats': { '$each': [newGroupChatID] } } }
            );
        }  
    },

    broadcast: function(message) {
        var user = Meteor.userId();
        var doc = UserData.findOne({'userID': user});
        var contacts = doc['contacts'];

        console.log('contacts');
        
        var chats = []
        for (var i = 0; i < contacts.length; ++i) {
            if (contacts[i] === user) {
                continue;
            }
            var chatDoc = ChatData.findOne(
                {'participants': [user, contacts[i]]}
            );
            if (!chatDoc) {
                chatDoc = ChatData.findOne(
                    { 'participants': [contacts[i], user] }
                );
            }
            

            if (chatDoc) {
                console.log(contacts[i]);
                Meteor.call('addTextToChat', chatDoc['_id'], message, user);
            }
            
        }
    }
})
