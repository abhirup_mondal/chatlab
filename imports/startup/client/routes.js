import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '../../ui/layout/primary.js';
import '../../ui/layout/sign-in.js';
import '../../ui/layout/sign-up.js';

import '../../ui/templates/chat-list-template.js';
import '../../ui/templates/chat-template.js';
import '../../ui/templates/profile-template.js';

FlowRouter.route('/',{
    name: 'primary',
    action() {
        if (Meteor.user() !== null) {
            BlazeLayout.render('primary', {main: 'chat-template', sidebar: 'chat-list-template'});
        }
        else {
            BlazeLayout.render('primary', {main: 'chat-template'});
        }
    }
});

FlowRouter.route('/chat/:chatID', {
    name: 'chatView',
    action: function(params, queryParams) {
        if (Meteor.user() !== null) {
            BlazeLayout.render('primary', { main: 'chat-template', sidebar: 'chat-list-template' , mainview: 'chat-view-template' });
        }
        else {
            BlazeLayout.render('primary', { main: 'chat-template' });
        }
    }
});

FlowRouter.route('/sign-in', {
    name: 'sign-in',
    action() {
        BlazeLayout.render('sign-in')
    }
});

FlowRouter.route('/sign-up', {
    name: 'sign-up',
    action() {
        BlazeLayout.render('sign-up')
    }
});

FlowRouter.route('/profile', {
    name: 'profile',
    action() {
        BlazeLayout.render('primary', {main:'profile-template'});
    }
});
