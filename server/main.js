import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';

import '../imports/methods.js';

import '../imports/collections/chatdata.js';
import '../imports/collections/userdata.js';

Meteor.publish('userDB', function () {
  return Meteor.users.find();
});

Meteor.startup(() => {
  // code to run on server at startup
});
